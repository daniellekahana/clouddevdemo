package services;

import java.util.ArrayList;
import java.util.Vector;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.stereotype.Service;

import entities.Book;



@Service
public class BooksService implements IBookService {
	
	private static BooksService instanceBooksService = new BooksService();
	private Vector<Book> books;
	
	private BooksService() {
		books = new Vector<Book>();
	}
	
	public static BooksService getInstance() {
		return instanceBooksService;
	}
	
	@Override
	public Book addBook(Book book) throws NullPointerException{
		
		if(book != null) {
			if(book.getIsbn() != null) {
				books.add(book);
				return book;
			}
		}
		
		throw new NullPointerException("error from function add book: Book is null.");
	}
	
	@Override
	public String[] getAllBooksIsbn() {
		ArrayList<String> booksIsbn = new ArrayList<>();
		
		for(Book book: books) {
			booksIsbn.add(book.getIsbn());	
		}
		return booksIsbn.toArray(new String[0]);
	}
	
	@Override
	public String[] getAllBooksTitle() {
		ArrayList<String> booksTitle = new ArrayList<String>();
		
		for(Book book: books) {
			booksTitle.add(book.getTitle());
		}
		return booksTitle.toArray(new String[0]);
	}
	
	@Override
	public String[] getAllBooksDetails() {
		ArrayList<String> booksDetails = new ArrayList<String>();
		
		for(Book book: books) {
			booksDetails.add(book.toString());
		}
		return booksDetails.toArray(new String[0]);
	}
	
	@Override
	public Book getBookByIsbn(String isbn) throws NullPointerException, NotFound{
		if(isbn == null) {
			throw new NullPointerException("error from function getBookByIsbn: Isbn is null.");
			
		} else if (isbn.length() < 10 || isbn.length() > 13) {
			throw new IllegalArgumentException("The isbn "+isbn+" is not between 10 to 13 chars.");
			
		}
		
		for(Book book: books) {
			if(book.getIsbn().equals(isbn)) {
				return book;
			}
		}
		
		throw new NotFound();
	}
	
	@Override
	public void deleteAllBooks() {
		books.clear();
		
	}
	
	
	public Book[] getAllBooks() {
		return books.toArray(new Book[0]);
		
	}


	

}
