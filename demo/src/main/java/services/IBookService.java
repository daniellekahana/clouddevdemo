package services;


import org.omg.CosNaming.NamingContextPackage.NotFound;

import entities.Book;

public interface IBookService {

	Book addBook(Book book);
	String[] getAllBooksIsbn() ;
	String[] getAllBooksTitle();
	String[] getAllBooksDetails();
	Book[] getAllBooks();
	Book getBookByIsbn(String isbn) throws NullPointerException, NotFound;
	void deleteAllBooks();
	
}

